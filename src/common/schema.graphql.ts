import { ObjectType, Field } from '@nestjs/graphql';
import { ReadStream } from 'fs';

@ObjectType()
export class Message {
  @Field()
  message: string;
}

@ObjectType()
export class Url {
  @Field()
  url: string;
}

export interface FileUpload {
  filename: string;
  mimetype: string;
  encoding: string;
  createReadStream: () => ReadStream;
}
