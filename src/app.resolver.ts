import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { Message, Url } from './common/schema.graphql';
import { FileUploadInput } from './common/upload-file.input';
import { AppService } from './app.service';

@Resolver()
export class AppResolver {
  constructor(private readonly appService: AppService) {}
  @Query(() => Message)
  getHello() {
    return {
      message: 'Hello world!',
    };
  }

  @Mutation(() => Url)
  async uploadFile(@Args('input') input: FileUploadInput) {
    const url = await this.appService.fileUpload(input);
    return { url: url };
  }
}
