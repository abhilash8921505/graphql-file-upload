import { Injectable } from '@nestjs/common';
import { FileUploadInput } from './common/upload-file.input';
import { Readable } from 'stream';
import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';

@Injectable()
export class AppService {
  private client = new S3Client({
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    },
    region: process.env.S3_REGION,
  });

  getHello(): string {
    return 'Hello World!';
  }

  async fileUpload(input: FileUploadInput) {
    const { filename, mimetype, createReadStream } = await input.file;

    const fileBuffer = await this.streamToBuffer(createReadStream());
    const url = await this.uploadFile(filename.trim(), mimetype, fileBuffer);
    return url;
  }

  private async streamToBuffer(stream: Readable): Promise<Buffer> {
    const buffer: Uint8Array[] = [];

    return new Promise((resolve, reject) =>
      stream
        .on('error', (error) => reject(error))
        .on('data', (data) => buffer.push(data))
        .on('end', () => resolve(Buffer.concat(buffer))),
    );
  }

  private async uploadFile(
    fileName: string,
    mimetype: string,
    fileBuffer: Buffer,
  ): Promise<string> {
    const key = `sample/${Date.now()}-${fileName.trim()}`;
    const bucketName = process.env.S3_BUCKET;

    try {
      await this.client.send(
        new PutObjectCommand({
          Bucket: bucketName,
          Body: fileBuffer,
          ContentType: mimetype,
          Key: key,
          ACL: 'public-read',
        }),
      );
    } catch (error) {
      console.log(error);
      throw new Error('Error uploading file');
    }

    return `https://${bucketName}.s3.amazonaws.com/${key}`;
  }
}
