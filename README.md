## Observations

1. `uploadFile` function in app.service is taking almost 8s to upload a 1.2MB file on a 150MBps internet
2. `graphql-upload@14`, `@types/graphql-upload` versions are frozen. Latest versions are not compatible with nest.js
3. Set header in apollo client to `apollo-require-preflight: true`. Only then can we send the graphql request from the client
4. Altair graphql client can be used to send file. Native GraphQL doesn't have UI for file upload
5. Resources:
   1. [GitHub issue about graphql-upload and nest.js incompatibility](https://github.com/jaydenseric/graphql-upload/issues/358)
   2. [createReadStream() to S3 upload](https://dev.to/tugascript/nestjs-graphql-image-upload-to-a-s3-bucket-1njg) - convert stream to buffer and use it for s3 upload
